function mail_con_form_check(f) {

	var iCount = 0;
	if (f.question.value == "") {
		alert("お問合わせ内容を選択してください。");
		return false;
	}
	if (f.name1.value == "") {
		alert("ご担当者をご記入下さい。");
		return false;
	}
	if (f.name2.value == "") {
		alert("ご担当者をご記入下さい。");
		return false;
	}
	if (f.todofuken.value == "") {
	      alert("都道府県を選択してください。");
	      return false;
	}
	if (f.jyusho.value == "") {
	      alert("住所を入力してください。");
	      return false;
	}
	if (f.tel.value == "") {
	      alert("電話番号を入力してください。");
	      return false;
	}
	if (f.mailaddress.value == "") {
	      alert("Eメールアドレスを入力してください。");
	      return false;
	}
	if (f.mailaddress.value.indexOf('@')==-1) {
	      alert("Eメールアドレスを正しく入力してください。");
	      return false;
	}
	if (f.mailaddress.value.indexOf('.')==-1) {
	      alert("Eメールアドレスを正しく入力してください。");
	      return false;
	}
	if (f.coments.value == "") {
	      alert("お問合わせ内容をご記入ください。");
	      return false;
	}	

}