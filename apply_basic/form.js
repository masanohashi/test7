function mail_con_form_check(f) {

	var iCount = 0;
	if (f.name1.value == "") {
		alert("お名前をご記入ください。");
		return false;
	}
	if (f.name2.value == "") {
		alert("お名前をご記入ください。");
		return false;
	}
	if (f.todofuken.value == "") {
	      alert("都道府県をご記入ください。");
	      return false;
	}
	if (f.jyusho.value == "") {
	      alert("住所をご記入ください。");
	      return false;
	}
	if (f.tel.value == "") {
	      alert("電話番号をご入力ください。");
	      return false;
	}
	if (f.mailaddress.value == "") {
	      alert("E-mailアドレスをご記入ください。");
	      return false;
	}
	if (f.mailaddress.value.indexOf('@')==-1) {
	      alert("E-mailアドレスを正しくご記入ください。");
	      return false;
	}
	if (f.mailaddress.value.indexOf('.')==-1) {
	      alert("E-mailアドレスを正しくご記入ください。");
	      return false;
	}
}